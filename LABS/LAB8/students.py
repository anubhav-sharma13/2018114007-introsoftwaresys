from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify

app = Flask(_name_)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test4.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    roll_number = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(80), unique=True)    
    email = db.Column(db.String(120), unique=True)

    def _init_(self, roll_number, name, email):
        self.roll_number = roll_number
        self.name = name
        self.email = email

    def _repr_(self):
        return '<User %r>' % self.name

@app.route("/students/create", methods=["POST"])
def userAdd():
    roll_number=request.form['roll_number']
    name=request.form['name']
    email= request.form['email']
    db.create_all()
    new_person=User(roll_number,name, email)
    db.session.add(new_person)
    db.session.commit()
    temp ={}
    temp['status']=(type(new_person)==User)
    return jsonify(temp)

@app.route("/students")
def userFetch():
    db.create_all()
    allUsers=User.query.all()
    strf = '{students:['

    for user in allUsers:
        strf += '{rollnumber:'+ str(user.roll_number) + ',' + 'name:' + user.name + ',' + 'email:' + user.email + '},'
    strf += ']}'    
    return strf

@app.route("/students/delete", methods=["POST"])
def deleteUser():
    rollnumber=request.form['roll_number']
    obj=User.query.filter_by(user.roll_number==rollnumber)
    db.session.delete(obj)
    db.session.commit()
    temp ={}
    temp['status']=(type(obj)==User)
    return jsonify(temp)


if _name_ == "_main_":
    app.run(host='127.0.0.1', port=5000)