from flask import Flask, render_template, jsonify, request
from application import app
from flask_sqlalchemy import SQLAlchemy

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

@app.route('/lang', methods=['GET', 'POST'])
def langselect():
	lang = request.form['dropddlang']
	if lang == '0':
		return render_template('Experiment.html')
	elif lang == '1':
		db.create_all()
		cursor = db.engine.execute('SELECT * FROM sentences')
		data = cursor.fetchall()
		return render_template('Experiment.html', showanswer=-1, data01=lang, data02=data)
	elif lang == '2':
		db.create_all()
		cursor = db.engine.execute('SELECT * FROM sentenceshin')
		data = cursor.fetchall()
		return render_template('Experiment.html', showanswer=-1, data01=lang, data02=data)

@app.route('/sen', methods=['GET', 'POST'])
def senselect():
	lang = request.form['hidlang']
	sen = request.form['dropddsen']
	if sen == '---Select a sentence---':
		return render_template('Experiment.html')
	else:
		db.create_all()
		dataind = 0
		datalength = 0
		datasen1 = ''
		data = ''
		dataoptions = ''
		if lang == '1':
			cursor = db.engine.execute('SELECT * FROM sentences WHERE SENTENCE = "' + sen + '"')
			dataind = cursor.fetchall()[0][0]
			cursor = db.engine.execute('SELECT * FROM sentences WHERE SENTENCE = "' + sen + '"')
			datalength = int(cursor.fetchall()[0][2])
			cursor = db.engine.execute("SELECT * FROM sen" + str(dataind))
			datasen1 = cursor.fetchall()
			cursor = db.engine.execute('SELECT * FROM sentences')
			data = cursor.fetchall()
			cursor = db.engine.execute('SELECT * FROM options')
			dataoptions = cursor.fetchall()
		elif lang == '2':
			cursor = db.engine.execute('SELECT * FROM sentenceshin WHERE SENTENCE = "' + sen + '"')
			dataind = cursor.fetchall()[0][0]
			cursor = db.engine.execute('SELECT * FROM sentenceshin WHERE SENTENCE = "' + sen + '"')
			datalength = int(cursor.fetchall()[0][2])
			cursor = db.engine.execute("SELECT * FROM sen" + str(dataind) + 'hin')
			datasen1 = cursor.fetchall()
			cursor = db.engine.execute('SELECT * FROM sentenceshin')
			data = cursor.fetchall()
			cursor = db.engine.execute('SELECT * FROM optionshin')
			dataoptions = cursor.fetchall()
	return render_template('Experiment.html', showanswer=0, data11=sen, data12=dataind, data13=datasen1, data01=lang, data02=data, data15=datalength, dataopt=dataoptions)

@app.route('/checkAnswers', methods=['GET', 'POST'])
def check():
	checkans = request.form['hidans']
	lang = request.form['hidlang1']
	sen = request.form['hidsen']
	ind = request.form['hidind']
	senlen = int(request.form['hidlen'])
	db.create_all()
	datasen = ''
	data = ''
	dataoptions = ''
	if lang == '1':
		cursor = db.engine.execute('SELECT * FROM sen' + str(ind))
		datasen = cursor.fetchall()
		cursor = db.engine.execute('SELECT * FROM sentences')
		data = cursor.fetchall()
		cursor = db.engine.execute('SELECT * FROM options')
		dataoptions = cursor.fetchall()
	elif lang == '2':
		cursor = db.engine.execute('SELECT * FROM sen' + str(ind) + 'hin')
		datasen = cursor.fetchall()
		cursor = db.engine.execute('SELECT * FROM sentenceshin')
		data = cursor.fetchall()
		cursor = db.engine.execute('SELECT * FROM optionshin')
		dataoptions = cursor.fetchall()
	dicti = []
	strtup = []
	for i in range(0, senlen):
		strind = str(i+1)
		string = request.form[strind]
		strtup.append(string)
		if datasen[i][2] == string:
			dicti.append('1')
		else:
			dicti.append('0')
	tup = tuple(dicti)
	strpass = tuple(strtup)
	if checkans == '0' or checkans == '2':
		return render_template('Experiment.html', showanswer=1, data01=lang, data11=sen, data02=data, data12=ind, data15=senlen, data2=tup, data13=datasen, data3=strpass, dataopt=dataoptions)
	else:
		return render_template('Experiment.html', showanswer=2, data01=lang, data11=sen, data02=data, data12=ind, data15=senlen, data2=tup, data13=datasen, data3=strpass, dataopt=dataoptions)
