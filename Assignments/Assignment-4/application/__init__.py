from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index_page():
	return render_template('Introduction.html'), 200

@app.route('/Introduction.html')
def introduction():
	return render_template('Introduction.html'), 200

@app.route('/Theory.html')
def theory():
	return render_template('Theory.html'), 200

@app.route('/Objective.html')
def objective():
	return render_template('Objective.html'), 200

@app.route('/Experiment.html')
def experiment():
	return render_template('Experiment.html'), 200

@app.route('/Quizzes.html')
def quizzes():
	return render_template('Quizzes.html'), 200

@app.route('/Procedure.html')
def procedure():
	return render_template('Procedure.html'), 200

@app.route('/Further_Readings.html')
def further_readings():
	return render_template('Further_Readings.html'), 200

@app.route('/Feedback.html')
def feedback():
	return render_template('Feedback.html'), 200
